# GPU Based Ribbons

This repository contains a couple off (in development) solutions to generating
ribbons on the GPU. This was created to research different methods, using GL
3.3+. 

## Compiling

*Mac and Linux*

    cd build
    ./release.sh
    
*Windows, using PowerShell* 

    cd build
    .\release.ps1   


## gpu-ribbons-2d-v2.cpp

![Alt text](screenshots/gpu-ribbons-v2.png "")
