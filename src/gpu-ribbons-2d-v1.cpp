/*
   ---------------------------------------------------------------
                                                     
                                                 oooo
                                                 '888
                  oooo d8b  .ooooo.  oooo    ooo  888  oooo  oooo
                  '888""8P d88' '88b  '88b..8P'   888  '888  '888
                   888     888   888    Y888'     888   888   888
                   888     888   888  .o8"'88b    888   888   888
                  d888b    'Y8bod8P' o88'   888o o888o  'V88V"V8P'
  
                                                    www.roxlu.com
                                            www.twitter.com/roxlu
  
  ---------------------------------------------------------------

  GPU BASED RIBBONS
  ==================

  - Generates a buffer with "history" positions for particles.
  - This history buffer is used to create ribbons.
  - Uses a ping/pong'd particle buffer (xfb_particle_buffers).
  - Uses a separate buffer to store the positions for the ribbons. 
  - The ribbon positions are stored like this:

                            frame 1      frame 2      frame 3
      indices for ribbon 0: [0, 3, 6],   [3, 6, 0],   [6, 0, 3]
      indices for ribbon 1: [1, 4, 7],   [4, 7, 1],   [7, 1, 4]
      indices for ribbon 2: [2, 5, 8],   [5, 8, 2],   [8, 2, 5]
      
      Positions for particles A, B, C at time steps 0, 1, 2:
      [A0, B0, C0], [A1, B1, C1], [A2, B2, C2]

  - For each particle simulation step we store the last position
    sequentially in a buffer. 

  - We generate an index buffer with the indices for all the time steps.  For
    example when we have 2 particles and a line strip of 3 segments, we would
    STORE the vertex positions in the ribbon buffer like this:

      [pos-particle-a-frame-0], index = 0 <--
      [pos-particle-b-frame-0], index = 1
      [pos-particle-a-frame-1], index = 2 <--
      [pos-particle-b-frame-1], index = 3
      [pos-particle-a-frame-2], index = 4 <--
      [pos-particle-b-frame-2], index = 5

    To generate a line strip we would need to create a list like [0, 2, 4] for
    the first particle and for the second one [1, 3, 5].  When we want to render
    a line strip of 3 segments we have to pick the correct index into which we
    write at frame 4. As you can see in the list above we start by writing the
    first position at index 0. But for the 4th run, we have to keep indices 2
    and 4 (for particle 1) as they are and write the new position into index
    0. This works as a ring buffer.

   - So we pre-calculate the indices in the `ribbon_index_buffer`. For each
     possible order we create the indices that are used to render the line
     strips. Following the above description where the ribbon for particle 1
     uses the indices [0, 2, 4] for the first time, it uses [2, 4, 0] for the
     second time and [4, 0, 2] for the third time. After that we start over.

   - Because we have to generate this list of indices for each of the particles,
     the ribbon_index_buffer becomes pretty big. E.g. for two particles with 
     3 segments we have:

       1st: [0, 2, 4][1, 3, 5], 
       2nd: [2, 4, 0][3, 5, 1],
       3rd: [4, 0, 2][5, 1, 3]

       All these indices are stored sequentially in the indices vbo.
       When rendering we offset to the right position (e.g. start of 
       1st, 2nd, 3rd).

 */
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include <vector>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

void button_callback(GLFWwindow* win, int bt, int action, int mods);
void cursor_callback(GLFWwindow* win, double x, double y);
void key_callback(GLFWwindow* win, int key, int scancode, int action, int mods);
void char_callback(GLFWwindow* win, unsigned int key);
void error_callback(int err, const char* desc);
void resize_callback(GLFWwindow* window, int width, int height);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

/* -------------------------------------------- */

int win_w = 2160;
int win_h = 1280;

/* -------------------------------------------- */

int main() {

  glfwSetErrorCallback(error_callback);

  if(!glfwInit()) {
    printf("Error: cannot setup glfw.\n");
    exit(EXIT_FAILURE);
  }

  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_COCOA_RETINA_FRAMEBUFFER, GL_FALSE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  
  GLFWwindow* win = NULL;
  int w = win_w;
  int h = win_h;

  win = glfwCreateWindow(w, h, "GPU Ribbons v1.", NULL, NULL);
  if(!win) {
    glfwTerminate();
    exit(EXIT_FAILURE);
  }

  glfwSetFramebufferSizeCallback(win, resize_callback);
  glfwSetKeyCallback(win, key_callback);
  glfwSetCharCallback(win, char_callback);
  glfwSetCursorPosCallback(win, cursor_callback);
  glfwSetMouseButtonCallback(win, button_callback);
  glfwSetScrollCallback(win, scroll_callback);
  glfwMakeContextCurrent(win);
  glfwSwapInterval(1);
  
  if (!gladLoadGL()) {
    printf("Cannot load GL.\n");
    exit(1);
  }

  // ----------------------------------------------------------------
  // THIS IS WHERE YOU START CALLING OPENGL FUNCTIONS, NOT EARLIER!!
  // ----------------------------------------------------------------

  /* PARTICLE SIMULATION */
  /* ---------------------------------- */
  
  int particle_count = 500;
  int particle_stride = sizeof(float) * 2; /* 2 = pos */
  int particle_bufsize = particle_count * particle_stride;
  float* particle_data = (float*)malloc(particle_bufsize);
  for (int i = 0; i < particle_count; ++i) {
    int dx = i * 2;
    particle_data[dx + 0] = 100 + i * 4;
    particle_data[dx + 1] = 0;
  }

  const char* xfb_particle_shader_source = ""
    "#version 330\n"
    "\n"
    "layout (location = 0) in vec2 in_position;\n"
    "\n"
    "out vec2 out_position;\n"
    "\n"
    "void main() {\n"
    " out_position = in_position + vec2(0.0, 5.0);\n"
    "}"
    "";

  GLuint xfb_particle_shader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(xfb_particle_shader, 1, (const char**)&xfb_particle_shader_source, NULL);
  glCompileShader(xfb_particle_shader);
  
  GLuint xfb_particle_prog = glCreateProgram();
  glAttachShader(xfb_particle_prog, xfb_particle_shader);

  const char* xfb_particle_varyings[]= { "out_position"  };
  glTransformFeedbackVaryings(xfb_particle_prog, 1, xfb_particle_varyings, GL_INTERLEAVED_ATTRIBS);
  glLinkProgram(xfb_particle_prog);

  /* Buffers for particle xfb */
  GLuint xfb_particle_buffers[2] = { 0 };
  glGenBuffers(2, xfb_particle_buffers);
  for (int i = 0; i < 2; ++i) {
    glBindBuffer(GL_ARRAY_BUFFER, xfb_particle_buffers[i]);
    glBufferData(GL_ARRAY_BUFFER, particle_bufsize, particle_data, GL_DYNAMIC_COPY);
  }  

  /* Vertex Arrays to ping pong. */
  GLuint xfb_particle_vaos[2] = { 0 };
  glGenVertexArrays(2, xfb_particle_vaos);
  for (int i = 0; i < 2; ++i) {
    glBindVertexArray(xfb_particle_vaos[i]);
    glBindBuffer(GL_ARRAY_BUFFER, xfb_particle_buffers[1 - i]);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, particle_stride, (GLvoid*)0);
  }

  int xfb_particle_dx = 0;

  /* RIBBONS 

    - Going to store position data at time step A, B, C like this:
      [posA0, posA1, posA2], [posB0, posB1, posB2], [posC0, posC1, posC2], etc..
   */

  int ribbon_max_frames = 100;
  int ribbon_stride = sizeof(float) * 2 * particle_count; /* 2 = pos */
  int ribbon_bufsize = ribbon_stride * ribbon_max_frames;
  uint8_t* ribbon_data = (uint8_t*) malloc(ribbon_bufsize);

  /* Buffer that stores the ribbon data; i.e. a list of old positions. */
  GLuint ribbon_buffer = 0;
  glGenBuffers(1, &ribbon_buffer);
  glBindBuffer(GL_ARRAY_BUFFER, ribbon_buffer);
  glBufferData(GL_ARRAY_BUFFER, ribbon_bufsize, ribbon_data, GL_DYNAMIC_COPY);

  /* Ribbon XFB */
  const char* ribbon_shader_source = ""
    "#version 330\n"
    "\n"
    "layout (location = 0) in vec2 in_particle_position;\n"
    "\n"
    "out vec2 out_ribbon_position;\n"
    "\n"
    "void main() {\n"
    "  out_ribbon_position = in_particle_position;\n"
    "}";

  GLuint ribbon_shader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(ribbon_shader, 1, (const char**)&ribbon_shader_source, NULL);
  glCompileShader(ribbon_shader);

  GLuint ribbon_prog = glCreateProgram();
  glAttachShader(ribbon_prog, ribbon_shader);

  const char* ribbon_varyings[] = { "out_ribbon_position" };
  glTransformFeedbackVaryings(ribbon_prog, 1, ribbon_varyings, GL_INTERLEAVED_ATTRIBS);
  glLinkProgram(ribbon_prog);
  
  GLuint ribbon_vaos[2] = { 0 };
  for (int i = 0; i < 2; ++i) {
    glGenVertexArrays(1, &ribbon_vaos[i]);
    glBindVertexArray(ribbon_vaos[i]);
    glBindBuffer(GL_ARRAY_BUFFER, xfb_particle_buffers[i]);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, particle_stride, (GLvoid*)0);
  }

  /* Ribbon index buffer. */
  int indices_per_frame = particle_count * ribbon_max_frames;
  int indices_buf_size = ribbon_max_frames * indices_per_frame;
  std::vector<int> ribbon_indices;

  for (int history = 0; history < ribbon_max_frames; ++history) {
    for (int particle = 0; particle < particle_count; ++particle) {
      for (int frame = 0; frame < ribbon_max_frames; ++frame) {
        int dx = (history * particle_count) + (frame * particle_count) + particle; /* [(0, 2, 4, 6),(1, 3, 5, 7)], [(2, 4, 6, 0), (1, 3, 5, 7)] */
        dx = dx % indices_per_frame;
        ribbon_indices.push_back(dx);
      }
    }
  }

  GLuint ribbon_index_buffer = 0;
  glGenBuffers(1, &ribbon_index_buffer);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ribbon_index_buffer);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, ribbon_indices.size() * sizeof(unsigned int), &ribbon_indices[0], GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  int ribbon_dx = 0;
  int ribbon_frame = 0;
  int ribbon_buffer_offset = 0;

  /* Drawing */
  const char* draw_vss = ""
    "#version 330\n"
    "\n"
    "uniform mat4 u_pm;\n"
    "\n"
    "layout (location = 0) in vec2 a_position;\n"
    "\n"
    "void main() {\n"
    "  vec4 p = vec4(a_position, 0.0, 1.0);\n"
    "  gl_Position = u_pm * p;\n"
    "}";

  const char* draw_fss = ""
    "#version 330\n"
    "\n"
    "layout (location = 0) out vec4 fragcolor;\n"
    "\n"
    "void main() {\n"
    "  fragcolor = vec4(1.0, 1.0, 1.0, 1.0);\n"
    "}";

  GLuint draw_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(draw_vertex_shader, 1, (const char**)&draw_vss, NULL);
  glCompileShader(draw_vertex_shader);

  GLuint draw_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(draw_fragment_shader, 1, (const char**)&draw_fss, NULL);
  glCompileShader(draw_fragment_shader);

  GLuint draw_prog = glCreateProgram();
  glAttachShader(draw_prog, draw_vertex_shader);
  glAttachShader(draw_prog, draw_fragment_shader);
  glLinkProgram(draw_prog);
  // poly_print_program_link_info(draw_prog);

  float pm[4][4] = {
    { 2.0f / win_w,         0.0f,   0.0f, 0.0f},
    {         0.0f,  2.0f/-win_h,   0.0f, 0.0f},
    {         0.0f,          0.0f, -1.0f, 0.0f},
    {        -1.0f,          1.0f,  0.0f, 1.0f}
  };
  
  GLint u_pm = glGetUniformLocation(draw_prog, "u_pm");
  glUseProgram(draw_prog);
  glUniformMatrix4fv(u_pm, 1, GL_FALSE, &pm[0][0]);

  GLuint draw_vao;
  glGenVertexArrays(1, &draw_vao);
  glBindVertexArray(draw_vao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ribbon_index_buffer);
  glBindBuffer(GL_ARRAY_BUFFER, ribbon_buffer);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 8, (GLvoid*)0);
  
  /* ---------------------------------- */

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  uint64_t frame_count = 0;
  
  while(!glfwWindowShouldClose(win)) {

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, w, h);
    glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /* Bind the read-vao and write-vbo for the particles. */
    glBindVertexArray(xfb_particle_vaos[xfb_particle_dx]); 
    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, xfb_particle_buffers[xfb_particle_dx]);
    xfb_particle_dx = 1 - xfb_particle_dx;

    /* Perform particle simulation step. */
    glUseProgram(xfb_particle_prog);
    glEnable(GL_RASTERIZER_DISCARD);
    glBeginTransformFeedback(GL_POINTS);
    glDrawArrays( GL_POINTS, 0, particle_count);
    glEndTransformFeedback();
    glDisable(GL_RASTERIZER_DISCARD);
    
    /* Bind the read-vao and write-vbo for the ribbons. */
    glBindVertexArray(ribbon_vaos[ribbon_dx]);
    glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, ribbon_buffer, ribbon_buffer_offset, ribbon_stride);
    glUseProgram(ribbon_prog);
    glEnable(GL_RASTERIZER_DISCARD);
    glBeginTransformFeedback(GL_POINTS);
    glDrawArrays(GL_POINTS, 0, particle_count);
    glEndTransformFeedback();
    glDisable(GL_RASTERIZER_DISCARD);

    /* Draw */
    glBindVertexArray(draw_vao);
    glUseProgram(draw_prog);

    if (frame_count > ribbon_max_frames) {
      for (int i = 0; i < particle_count; ++i) {
        int frame_offset = ribbon_frame * indices_per_frame;
        int particle_offset = i * ribbon_max_frames;
        int index_offset = frame_offset + particle_offset;
        int offset = index_offset * sizeof(unsigned int);
        glDrawElements(GL_LINE_STRIP, ribbon_max_frames, GL_UNSIGNED_INT, (GLvoid*)offset);
      }
    }
    
    ribbon_dx = 1 - ribbon_dx;
    ribbon_frame = (ribbon_frame + 1) % ribbon_max_frames;
    ribbon_buffer_offset = ribbon_frame * ribbon_stride;
    frame_count++;

    glfwSwapBuffers(win);
    glfwPollEvents();
  }

  glfwTerminate();

  return EXIT_SUCCESS;
}

void char_callback(GLFWwindow* win, unsigned int key) {
}

void key_callback(GLFWwindow* win, int key, int scancode, int action, int mods) {

  if (GLFW_RELEASE == action) {
    return;
  }
  switch(key) {
    case GLFW_KEY_SPACE: {
      break;
    }
    case GLFW_KEY_ESCAPE: {
      glfwSetWindowShouldClose(win, GL_TRUE);
      break;
    }
  };
}

void resize_callback(GLFWwindow* window, int width, int height) {
}

void cursor_callback(GLFWwindow* win, double x, double y) {
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
}

void button_callback(GLFWwindow* win, int bt, int action, int mods) {

  double mx = 0.0;
  double my = 0.0;
  glfwGetCursorPos(win, &mx, &my);

  if (GLFW_PRESS == action) {
  }
  else if (GLFW_RELEASE == action) {
  }
}

void error_callback(int err, const char* desc) {
  printf("GLFW error: %s (%d)\n", desc, err);
}

/*
  SX_DEBUG("Bound ribbon buffer. offset: %d, stride: %d, frame: %d", ribbon_buffer_offset, ribbon_stride, ribbon_frame);
*/

