#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <algorithm>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

void button_callback(GLFWwindow* win, int bt, int action, int mods);
void cursor_callback(GLFWwindow* win, double x, double y);
void key_callback(GLFWwindow* win, int key, int scancode, int action, int mods);
void char_callback(GLFWwindow* win, unsigned int key);
void error_callback(int err, const char* desc);
void resize_callback(GLFWwindow* window, int width, int height);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

/* -------------------------------------------- */

int win_w = 2160;
int win_h = 1440;
bool do_step = false;

/* -------------------------------------------- */

int main() {

  glfwSetErrorCallback(error_callback);

  if(!glfwInit()) {
    printf("Error: cannot setup glfw.\n");
    exit(EXIT_FAILURE);
  }

  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_COCOA_RETINA_FRAMEBUFFER, GL_FALSE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  
  GLFWwindow* win = NULL;
  int w = win_w;
  int h = win_h;

  win = glfwCreateWindow(w, h, "GPU Ribbons v2. (Use SPACE to toggle simulation)", NULL, NULL);
  if(!win) {
    glfwTerminate();
    exit(EXIT_FAILURE);
  }

  glfwSetFramebufferSizeCallback(win, resize_callback);
  glfwSetKeyCallback(win, key_callback);
  glfwSetCharCallback(win, char_callback);
  glfwSetCursorPosCallback(win, cursor_callback);
  glfwSetMouseButtonCallback(win, button_callback);
  glfwSetScrollCallback(win, scroll_callback);
  glfwMakeContextCurrent(win);
  glfwSwapInterval(1);
  
  if (!gladLoadGL()) {
    printf("Cannot load GL.\n");
    exit(1);
  }

  // ----------------------------------------------------------------
  // THIS IS WHERE YOU START CALLING OPENGL FUNCTIONS, NOT EARLIER!!
  // ----------------------------------------------------------------

  /* Description for our ribbons */
  int num_particles = 110;
  int num_points = 300;
  int num_indices_per_particle = (num_points - 2) * 2 + 2;
  int num_indices_to_create = num_indices_per_particle * num_particles;
  int num_total = num_indices_per_particle * num_particles;
  int num_positions = num_points * num_particles; /* How many positions we should store in the position history buffer. */

  /* ==================================================== */
  /* Ribbon indices                                       */
  /* ==================================================== */

  const char* xfb_vss = ""
    "#version 330\n"
    "\n"
    "uniform int u_round_trip;\n"
    "uniform int u_num_points;\n"
    "uniform int u_num_particles;\n"
    "\n"
    "out int out_index;\n"
    "\n"
    "void main() {\n"
    "  int num_indices_per_particle = (u_num_points - 2) * 2 + 2;\n"
    "  int particle = gl_VertexID / num_indices_per_particle;\n"
    "  int index = gl_VertexID % num_indices_per_particle;\n"
    "  int repeater = ((index + 1) / 2) + u_round_trip;\n"
    "  out_index = (repeater * u_num_particles + particle) % (u_num_points * u_num_particles);\n"
    "}";

  GLuint xfb_vs = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(xfb_vs, 1, (const char**)&xfb_vss, NULL);
  glCompileShader(xfb_vs);

  const char* xfb_vars[] = { "out_index" };
  GLuint xfb_prog = glCreateProgram();
  glAttachShader(xfb_prog, xfb_vs);
  glTransformFeedbackVaryings(xfb_prog, 1, xfb_vars, GL_INTERLEAVED_ATTRIBS);
  glLinkProgram(xfb_prog);

  GLint u_round_trip = glGetUniformLocation(xfb_prog, "u_round_trip");
  GLint u_num_points = glGetUniformLocation(xfb_prog, "u_num_points");
  GLint u_num_particles = glGetUniformLocation(xfb_prog, "u_num_particles");
  
  glUseProgram(xfb_prog);
  glUniform1i(u_round_trip, 0);
  glUniform1i(u_num_points, num_points);
  glUniform1i(u_num_particles, num_particles);

  /* A buffer that will be generated using xfb_prog. */
  GLuint ribbon_ebo = 0;
  glGenBuffers(1, &ribbon_ebo);
  glBindBuffer(GL_ARRAY_BUFFER, ribbon_ebo);
  glBufferData(GL_ARRAY_BUFFER, num_indices_to_create * sizeof(unsigned int), NULL, GL_DYNAMIC_COPY);

  GLuint ribbon_vao = 0;
  glGenVertexArrays(1, &ribbon_vao);
  glBindVertexArray(ribbon_vao);
  glBindBuffer(GL_ARRAY_BUFFER, ribbon_ebo);

  /* ==================================================== */
  /* Particle sim                                         */
  /* ==================================================== */

  const char* sim_vss = ""
    "#version 330\n"
    "layout (location = 0) in vec2 in_position;\n"
    "out vec2 out_position;\n"
    "void main() {\n"
    "  out_position = in_position + vec2(0.0, 5.0);\n"
    "}"
    "";

  GLuint sim_vert = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(sim_vert, 1, (const char**)&sim_vss, NULL);
  glCompileShader(sim_vert);

  GLuint sim_prog = glCreateProgram();
  glAttachShader(sim_prog, sim_vert);

  const char* sim_vars[] = { "out_position" };
  glTransformFeedbackVaryings(sim_prog, 1, sim_vars, GL_INTERLEAVED_ATTRIBS);
  glLinkProgram(sim_prog);

  GLuint sim_bufs[2] = { 0 };
  size_t sim_nbytes = num_particles * sizeof(float) * 2;
  uint8_t* sim_data = (uint8_t*) malloc(sim_nbytes);
  if (nullptr == sim_data) {
    printf("Failed to allocate: %zu bytes. (exiting).\n", sim_nbytes);
    exit(EXIT_FAILURE);
  }

  memset((char*)sim_data, 0x00, sim_nbytes);
  float* sim_data_pos = (float*)sim_data;
  for (size_t i = 0; i < num_particles; ++i) {
    sim_data_pos[i * 2 + 0] = 10 + i * 20;
    sim_data_pos[i * 2 + 1] = 0;
  }
  
  for (int i = 0; i < 2; ++i) {
    char name[256] = { 0 };
    sprintf(name, "Sim VBO-%d", i);
    glGenBuffers(1, &sim_bufs[i]);
    glBindBuffer(GL_ARRAY_BUFFER, sim_bufs[i]);
    glBufferData(GL_ARRAY_BUFFER, sim_nbytes, sim_data, GL_DYNAMIC_COPY);
  }

  GLuint sim_vaos[2] = { 0 };
  for (int i = 0; i < 2; ++i) {
    glGenVertexArrays(1, &sim_vaos[i]);
    glBindVertexArray(sim_vaos[i]);
    glBindBuffer(GL_ARRAY_BUFFER, sim_bufs[1 - i]);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 8, (GLvoid*)0); 
  }

  /* ==================================================== */
  /* Ribbon buffer                                        */
  /* ==================================================== */

  const char* rib_vss = ""
    "#version 330\n"
    "layout (location = 0) in vec2 in_position;\n"
    "out vec2 out_position;\n"
    "void main() {\n"
    "  out_position = in_position;\n"
    "}";
  
  GLuint rib_vert = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(rib_vert, 1, (const char**)&rib_vss, NULL);
  glCompileShader(rib_vert);

  GLuint rib_prog = glCreateProgram();
  glAttachShader(rib_prog, rib_vert);

  const char* rib_vars[] = { "out_position" };
  glTransformFeedbackVaryings(rib_prog, 1, rib_vars, GL_INTERLEAVED_ATTRIBS);
  glLinkProgram(rib_prog);

  int rib_stride = num_particles * 2 * sizeof(float); /* How many bytes per history frame. */
  int rib_nbytes = rib_stride * num_points; /* The number of bytes we need to store a full ribbon. */
  GLuint rib_buf = 0;
  
  glGenBuffers(1, &rib_buf);
  glBindBuffer(GL_ARRAY_BUFFER, rib_buf);
  glBufferData(GL_ARRAY_BUFFER, rib_nbytes, NULL, GL_DYNAMIC_COPY);

  GLuint rib_vaos[2] = { 0 };
  for (int i = 0; i < 2; ++i) {
    glGenVertexArrays(1, &rib_vaos[i]);
    glBindVertexArray(rib_vaos[i]);
    glBindBuffer(GL_ARRAY_BUFFER, sim_bufs[i]);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 8, (GLvoid*)0);
  }
  
  /* ==================================================== */
  /* Draw shader                                          */
  /* ==================================================== */
  
  const char* draw_vss = ""
    "#version 330\n"
    "uniform mat4 u_pm;\n"
    "layout (location = 0) in vec2 a_pos;\n"
    "void main() {"
    "  gl_Position = u_pm * vec4(a_pos, 0.0, 1.0);\n"
    "}";

  GLuint draw_vs = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(draw_vs, 1, (const char**)&draw_vss, NULL);
  glCompileShader(draw_vs);

  const char* draw_fss = ""
    "#version 330\n"
    "layout (location = 0) out vec4 fragcolor;\n"
    "void main() {\n"
    "  fragcolor = vec4(1.0, 1.0, 1.0, 1.0);\n"
    "}";

  GLuint draw_fs = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(draw_fs, 1, (const char**)&draw_fss, NULL);
  glCompileShader(draw_fs);

  GLuint draw_prog = glCreateProgram();
  glAttachShader(draw_prog, draw_vs);
  glAttachShader(draw_prog, draw_fs);
  glLinkProgram(draw_prog);

  GLint u_pm = glGetUniformLocation(draw_prog, "u_pm");

  float pm[16] = { 0 };
  pm[0] = 2.0f / win_w;
  pm[5] = 2.0f / -win_h;
  pm[10] = -1.0f;
  pm[12] = -1.0f;
  pm[13] = 1.0f;
  pm[15] = 1.0f;
  
  glUseProgram(draw_prog);
  glUniformMatrix4fv(u_pm, 1, GL_FALSE, pm);

  /* ==================================================== */
  /* Drawing  particles                                   */
  /* ==================================================== */
  
  GLuint draw_particles_vaos[2] = { 0 };
  for (int i = 0; i < 2; ++i) {
    glGenVertexArrays(1, &draw_particles_vaos[i]);
    glBindVertexArray(draw_particles_vaos[i]);
    glBindBuffer(GL_ARRAY_BUFFER, sim_bufs[i]);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 8, (GLvoid*)0);
  }

  /* ==================================================== */
  /* Drawing ribbons                                      */
  /* ==================================================== */

  GLuint draw_rib_vao = 0;
  glGenVertexArrays(1, &draw_rib_vao);
  glBindVertexArray(draw_rib_vao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ribbon_ebo);
  glBindBuffer(GL_ARRAY_BUFFER, rib_buf);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 8, (GLvoid*)0);

  int sim_dx = 0;
  int frame_counter = 0;
  int history_dx = 0;
  int round_trip = -1;
  int prev_round_trip = 0;
  int num_indices_to_draw = 0;
  
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  while(!glfwWindowShouldClose(win)) {

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, w, h);
    glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (true == do_step) {

      /* Particle simulation */
      glUseProgram(sim_prog);
      glBindVertexArray(sim_vaos[sim_dx]);
      glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, sim_bufs[sim_dx]);
      glEnable(GL_RASTERIZER_DISCARD);
      glBeginTransformFeedback(GL_POINTS);
      glDrawArrays(GL_POINTS, 0, num_particles);
      glEndTransformFeedback();
      glDisable(GL_RASTERIZER_DISCARD);
    
      /* Store last position for ribbons */
      glUseProgram(rib_prog);
      glBindVertexArray(rib_vaos[sim_dx]);
      glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, rib_buf, history_dx * rib_stride,  rib_stride);
      glEnable(GL_RASTERIZER_DISCARD);
      glBeginTransformFeedback(GL_POINTS);
      glDrawArrays(GL_POINTS, 0, num_particles);
      glEndTransformFeedback();
      glDisable(GL_RASTERIZER_DISCARD);
      sim_dx = 1 - sim_dx;
      
      prev_round_trip = round_trip;
      round_trip = (frame_counter / num_points) % num_points; 
      history_dx = (history_dx + 1) % num_points;
      frame_counter++;

      if (frame_counter <= num_points) {
        /* 
           This will be executed when the position buffer hasn't been filled
           completely. Lets say we want to store 5 positions, the next lines
           will be executed 5 times. We will also make sure that the index
           buffer will only generate index values up to the total number of
           frames (i.e. points) that we created.
        */
        glUseProgram(xfb_prog);
        glUniform1i(u_num_points, frame_counter);
        glUniform1i(u_round_trip, frame_counter);
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, ribbon_ebo);
        glEnable(GL_RASTERIZER_DISCARD);
        glBeginTransformFeedback(GL_POINTS);
        glDrawArrays(GL_POINTS, 0, num_indices_to_create);
        glEndTransformFeedback();
        glDisable(GL_RASTERIZER_DISCARD);
        num_indices_to_draw = std::min<int>( ((frame_counter - 2) * 2 + 2) * num_particles, num_total);
      }
      else {
        glUseProgram(xfb_prog);
        glUniform1i(u_round_trip, frame_counter);
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, ribbon_ebo);
        glEnable(GL_RASTERIZER_DISCARD);
        glBeginTransformFeedback(GL_POINTS);
        glDrawArrays(GL_POINTS, 0, num_indices_to_create);
        glEndTransformFeedback();
        glDisable(GL_RASTERIZER_DISCARD);
      }
    }
    
    /* Draw ribbons */
    glUseProgram(draw_prog);
    glBindVertexArray(draw_rib_vao);
    glDrawElements(GL_LINES, num_indices_to_draw, GL_UNSIGNED_INT, (GLvoid*)0);
    
    /* Draw particles */
    glUseProgram(draw_prog);
    glBindVertexArray(draw_particles_vaos[1 - sim_dx]);
    glDrawArrays(GL_POINTS, 0, num_particles);
    
    glfwSwapBuffers(win);
    glfwPollEvents();
  }
  
  glfwTerminate();

  return EXIT_SUCCESS;
}

void char_callback(GLFWwindow* win, unsigned int key) {
}

void key_callback(GLFWwindow* win, int key, int scancode, int action, int mods) {

  if (GLFW_RELEASE == action) {
    return;
  }
  switch(key) {
    case GLFW_KEY_SPACE: {
      do_step = !do_step;
      break;
    }
    case GLFW_KEY_ESCAPE: {
      glfwSetWindowShouldClose(win, GL_TRUE);
      break;
    }
  };
}

void resize_callback(GLFWwindow* window, int width, int height) {
}

void cursor_callback(GLFWwindow* win, double x, double y) {
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
}

void button_callback(GLFWwindow* win, int bt, int action, int mods) {

  double mx = 0.0;
  double my = 0.0;
  glfwGetCursorPos(win, &mx, &my);

  if (GLFW_PRESS == action) {
  }
  else if (GLFW_RELEASE == action) {
  }
}

void error_callback(int err, const char* desc) {
  printf("GLFW error: %s (%d)\n", desc, err);
}

/*
  SX_DEBUG("Bound ribbon buffer. offset: %d, stride: %d, frame: %d", ribbon_buffer_offset, ribbon_stride, ribbon_frame);
*/
