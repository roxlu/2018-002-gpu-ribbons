#!/bin/sh

d=${PWD}
bd=${d}/compile

if [ ! -d ${bd} ] ; then
    mkdir ${bd}
fi

cd ${bd}

# Configure
cmake -DCMAKE_BUILD_TYPE=Release ..
if [ $? -ne 0 ] ; then
    echo "Failed to configure."
    exit
fi

# Build
cmake --build . 
if [ $? -ne 0 ] ; then
    echo "Failed to build."
    exit
fi

#./gpu-ribbons-2d-v1
#./gpu-ribbons-2d-v2
./primitive-restart
