$d = (Resolve-Path .\).Path +"/"
$bd = $d +"compile/"
$id = $d +"../install/"

Try {

  pushd $d
  
  if (! (Test-Path -Path $bd)) {
     Write-Host "Compile dir not found"
     New-Item -ItemType directory -Path $bd
  }
  
  cd compile
  cmake -DCMAKE_BUILD_TYPE=Release `
        -DCMAKE_INSTALL_PREFIX="$id" `
        -G "Visual Studio 15 2017 Win64" `
        ..
  
  cmake --build . --target install
  
  cd "$id/bin"
  # ./gpu-ribbons-2d-v1.exe
  ./gpu-ribbons-2d-v2.exe
}

Finally {
  popd
}